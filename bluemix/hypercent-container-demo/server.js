(function(){

	var express = require('express');
	var app = express();

	var ejs = require('ejs');
	app.set('view engine', 'ejs');

	var path = require('path');
	app.set('views', path.join(__dirname, 'www'));
	
	app.get('/', function(req, res){
		res.render('index', {
			companyName: 'Hypercent',
			topics: [
				'Docker',
				'Docker Hub',
				'Creating a docker container for a NodeJS application',
				'Pushing a Docker Image to a Registry',
				'Running a Container from your Docker Image'
			]
		});
	});

	app.get('/api/hello-world', function(req, res){
		res.json({ hello: 'world' });
	});

	var port = 6001;
	app.listen(port, function(error, result){
		if(error){
			console.log(error);
		}
		else{
			console.log('Listening on port ' + port);
		}
	});

})();