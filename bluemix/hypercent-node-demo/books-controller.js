module.exports = function(pool){

	var self = this;

	this.mysql = require('mysql');
	this.config = require('./config');
	this.pool = pool;

	this.get = function(req, res){
		var connection = self.pool.getConnection(function(error, connection){
			if(error){
				res.status(500);
				res.json(error);
			}
			else{
				connection.query(self.config.mysql.queries.selectBooks, function(error, rows){
					connection.release();

					if(error){
						res.json(error);
					}
					else{
						if(rows.length > 0){
							var books = [];
							for(var i=0; i < rows.length; i++){
								books.push({
									bookId: rows[i].BookId,
									title: rows[i].Title,
									author: rows[i].Author,
								});
							}

							res.json(books);
						}
						else{
							res.sendStatus(404);
						}
					}
				});
			}
		});		
	}

	this.post = function(req, res){
		var connection = self.pool.getConnection(function(error, connection){
			if(error){
				res.status(500);
				res.json(error);
			}
			else{
				connection.query(self.config.mysql.queries.insertBook, [
						req.body.title, req.body.author
					], function(error, result){
					connection.release();

					if(error){
						res.status(500);
						res.json(error);
					}
					else{
						res.json({
							bookId: result.insertId
						});
					}
				});
			}
		});	
	}
}