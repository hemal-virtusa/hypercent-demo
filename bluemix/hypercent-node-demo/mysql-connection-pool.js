module.exports = function(){
	var self = this;

	this.config = require('./config');
	
	this.createPool = function(){
		var mysql = require('mysql');
		return mysql.createPool(self.config.mysql.connectionPool);
	}
}